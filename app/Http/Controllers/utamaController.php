<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;


class utamaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = user::all();
      return view('dashboarddosen')->withusers($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function getDasboard()
     {
       $data = user::all();
       return view('dashboarddosen')->withusers($data);
     }

    public function create()
    {
          return view('createdosen');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
     {
        $data = User::create([
          'name' => $request->name,
          'email' => $request->email,
          'password' => bcrypt($request->password),
          'alamat' => $request->alamat,
          'no_hp' => $request->no_hp,
        ]);
        //$data = new User();
       return redirect()->route('cruddosen.index')->with('alert-success', 'Data Berhasil Ditambah');
     // Image::create($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = User::findOrFail($id);
      return view('editdosen', compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = User::findOrFail($id);
      $data->update([
        'name' => $request->name,
        'email' => $request->email,
        'password' => bcrypt($request->password),
        'alamat' => $request->alamat,
        'no_hp' => $request->no_hp,
      ]);
      $data->save();
      return redirect()->route('cruddosen.index')->with('alert-success', 'Data Berhasil Diubah.');

    }

    /**
     * Remove the specifie resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::findOrFail($id);
        $data->delete();
        return redirect()->route('cruddosen.index')->with('alert-success', 'Data Berhasil Dihapus.');
    }

    public function getTable()
    {
      $data = user::all();
      return view('listdosen')->withusers($data);
    }
}
