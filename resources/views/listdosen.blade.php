@extends('layout.app')
@section('content')
<style media="screen">
  .container{
    font-family: 'Roboto';
  }
  tr, th, h3, footer{
    font-family:'Roboto';
  }
  .btn{
    font-family: 'Roboto';
  }
  input{
    align:right;
  }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>List Dosen</h3>
            <div class="panel panel-default">
                <div class="panel-body">
                    Search: <input type="text" name="" value="">
                    <br><br>
                    <table class="table table-striped">
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>E-mail</th>
                            <th>Alamat</th>
                            <th>No. HP</th>
                        </tr>
                        <?php $no=1; ?>
                        @foreach( $users as $user )
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->alamat}}</td>
                            <td>{{$user->no_hp}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <footer><a href="{{url('/')}}">Back to Home</footer></a>
</div>
@endsection
