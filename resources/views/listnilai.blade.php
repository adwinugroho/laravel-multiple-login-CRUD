@extends('layout.app')
@section('content')
<style media="screen">
  .container{
    font-family: 'Roboto';
  }
  tr, th, h3, footer{
    font-family:'Roboto';
  }
  .btn{
    font-family: 'Roboto';
  }
  input{
    align:right;
  }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>List Nilai</h3>
            <div class="panel panel-default">
                <div class="panel-body">
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success">
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif
                    Search: <input type="text" name="" value="">
                    <br><br>
                    <table class="table table-striped">
                        <tr>
                            <th>No</th>
                            <th>ID Dosen</th>
                            <th>Nama Dosen</th>
                            <th>Jurusan</th>
                            <th>Nama Mahasiswa</th>
                            <th>Nilai Tugas</th>
                            <th>Nilai UTS</th>
                            <th>Nilai UAS</th>
                        </tr>
                        <?php $no=1; ?>

                                <form method="POST" action="" accept-charset="UTF-8">
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <footer><a href="{{url('/')}}">Back to Home</footer></a>
</div>
@endsection
