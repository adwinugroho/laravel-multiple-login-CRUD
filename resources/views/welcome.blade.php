<!DOCTYPE HTML>
<html>
	<head>
		<title>Sistem Informasi Nilai Online</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="{{asset('frontend/js/jquery.min.js')}}"></script>
		<script src="{{asset('frontend/js/skel.min.js')}}"></script>
		<script src="{{asset('frontend/js/skel-layers.min.js')}}"></script>
		<script src="{{asset('frontend/js/init.js')}}"></script>
		<link rel="stylesheet" href="{{asset('frontend/css/skel.css')}}" />
		<link rel="stylesheet" href="{{asset('frontend/css/style.css')}}" />
		<link rel="stylesheet" href="{{asset('frontend/css/style-xlarge.css')}}" />
	</head>
	<body id="top">

		<!-- Header -->
			<header id="header" class="skel-layers-fixed">
				<h1><a href="#">SINIO</a></h1>
				<nav id="nav">
					<ul>
						<li><a href="{{ url('/listdosen')}}">List Dosen</a></li>
						<li><a href="{{ url('/listnilai')}}">List Nilai</a></li>
					</ul>
			</header>

		<!-- Banner -->
			<section id="banner">
				<div class="inner">
					<h2>SINIO</h2>
					<p>Sistem Informasi Nilai Online UMM</p>
					<ul class="actions">
						@if (Route::has('login'))
							@if (Auth::check())
                @if (Auth::user()->admin)
                <li><a href="{{ route('cruddosen.index') }}" class="button big alt">Go To Dashboard</a></li>
                @else
                <li><a href="{{ url('/home') }}" class="button big alt">Go To Dashboard</a></li>
                @endif
              @else
							<li><a href="{{ url('/login') }}" class="button big alt">Login</a></li>
							<li><a href="{{ url('/register') }}" class="button big special">Register</a></li>
							@endif
						@endif
					</ul>
				</div>
			</section>
		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<div class="row double">
						<div class="6u">
							<div class="row collapse-at-2">
								<div class="6u">
								<h3>Contact Us</h3>
									<ul class="alt">
										<li><a href="#">address:</a><p>Jl. Raya Tlogomas No.246, Jawa Timur 65144, Indonesia</p></li>
										<li><a href="#">email:</a><p>SINO.UMM@gmail.com</p></li>
										<li><a href="#">phone:</a><p>(0341) 6276790 fast response WA: 0853-4842-7337</p></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="6u">
							<h2>About Us</h2>
							<p>SINIO Sistem Informasi Nilai Online Universitas Muhammadiyah Malang yang digunakan untuk memudahkan dosen untuk menginput nilai yang kemudian diserahkan ke BAA</p>
							<br>
							<p>Follow Us</p>
							<ul class="icons">
								<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
								<li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
								<li><a href="#" class="icon fa-pinterest"><span class="label">Pinterest</span></a></li>
							</ul>
						</div>
					</div>
					<ul class="copyright">
						<li>&copy; SINIO. All rights reserved.</li>
						<li>Design: <a href="http://templated.co">TEMPLATED</a></li>
						<li>Images: <a href="http://unsplash.com">Unsplash</a></li>
					</ul>
				</div>
			</footer>
	</body>
</html>
