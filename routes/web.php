<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::resource('cruddosen', 'utamaController');
Route::get('/listdosen', 'utamaController@getTable');

Route::post('/login/custom', [
  'uses' => 'LoginController@login',
  'as' => 'login.custom',
]);


Route::group(['middleware' => 'auth'], function(){
  Route::get('/home', function(){
    return view('home');
  })->name('home');;
  Route::get('/dashboarddosen', 'utamaController@index');

});

Route::get('/listnilai', function () {
    return view('listnilai');
})->name('listnilai');
